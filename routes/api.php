<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/registration', [UserController::class, 'registration']);
Route::post('/auth/login', [UserController::class, 'login'])->name('login');
Route::post('/auth/refresh_token', [UserController::class, 'refreshToken'])->name('refreshToken');
Route::post('/geolocation/save_coordinate', [UserController::class, 'saveCoordinate'])->middleware('client_credentials')->name('saveCoordinate');
Route::post('/geolocation/get_all_addresses', [UserController::class, 'getAllAddresses'])->middleware('client_credentials')->name('getAllAddress');


