<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeoRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Resources\UserAddressResource;
use App\Http\Resources\UserResource;
use App\Jobs\GeolocationJobs;
use App\Models\User;
use App\Models\UsersAddress;
use App\Services\UserService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Laravel\Passport\Client as OClient;
use Symfony\Component\HttpFoundation\Response as HTTP_RESPONSE;
use Throwable;
use Yandex\Geocode\Facades\YandexGeocodeFacade;
use function Ramsey\Uuid\v4;

class UserController extends Controller
{
    /**
     * @throws GuzzleException|Throwable
     */
    public function registration(RegistrationRequest $request, UserService $userService): JsonResponse
    {
        $email = $request['email'];
        $password = $request['password'];
        $user = UserResource::make(
            $userService->create(app(User::class)->fill([
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            ]))
        );
        $tokensData = $userService->getTokens($email, $password);
        return response()->json([
            'tokens' => $tokensData,
            'user' => $user,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function login(LoginRequest $request, UserService $userService): JsonResponse
    {
        $email = $request['email'];
        $password = $request['password'];

        $user = $userService->authorizedUserByPassword(
            $email,
            $password
        );
/*
        if (!$user) {
            return response()->json([
                "type" => "USER_IS_NOT_AUTHORIZED",
                "code" => HTTP_RESPONSE::HTTP_NOT_FOUND,
                "message" => "Пользователь с таким email не найден"
            ], HTTP_RESPONSE::HTTP_NOT_FOUND);
        }
        {
            $userService->authUser($user);
        }
        */
        Auth::attempt(['email' => $email,'password' =>$password]);
        $tokensData = $userService->getTokens($email, $password);
        return response()->json([
            'tokens' => $tokensData,
            'user' => $user,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function refreshToken(LoginRequest $request, UserService $userService): JsonResponse
    {
        $email = $request['email'];
        $password = $request['password'];

        $user = $userService->authorizedUserByPassword(
            $request->input('email'),
            $request->input('password')
        );
        $userService->authorizedUserByPassword($email, $password);
        $tokensData = $userService->getTokens($email, $password);
        $userService->refreshTokens($tokensData['refresh_token']);
        return response()->json([
            'tokens' => $tokensData,
            'user' => $user,
        ]);
    }

    public function saveCoordinate(GeoRequest $request): string
    {

        $longitude = $request['longitude'];
        $latitude = $request['latitude'];
        $userId = $request['user_id'];
        $id = v4();

        UsersAddress::create([
            'id' => $id,
            'user_id' => $userId,
            'longitude' => $longitude,
            'latitude' => $latitude
        ]);

        GeolocationJobs::dispatch($longitude, $latitude, $id)->onQueue('geo');

        return 'ok';
    }

    public function getAllAddresses(Request $request, UserService $userService): LengthAwarePaginator
    {
        $userId = $request['user_id'];
        return ($userService->getAllAddressesOfUser($userId));

    }

}
