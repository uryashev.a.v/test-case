<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserAddressResource extends ResourceCollection
{
    /**
     *
     * @property string $id
     * @property string userId
     * @property string longitude
     * @property string latitude
     * @property Carbon $created_at
     * @property Carbon $updated_at
     * @mixin Builder
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->userId,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
        /*return [
            'data' => $this->collection->map(fn ($item) => UserResource::make($item)),
        ];*/
    }
}
