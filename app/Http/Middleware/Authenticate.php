<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as HTTP_RESPONSE;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     * @return string|null
     */

    public function handle($request, Closure $next, ...$guards)
    {
        if (!Auth::check()) {
            return response()->json([
                "type" => "UNAUTHORIZED",
                "code" => HTTP_RESPONSE::HTTP_UNAUTHORIZED,
                "message" => "Пользователь не авторизован"
            ], HTTP_RESPONSE::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }

    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
