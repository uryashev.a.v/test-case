<?php

namespace App\Http\Requests;

use Closure;
use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
