<?php

namespace App\Http\Requests;


use Illuminate\Validation\Rules\Password;

class RegistrationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email:rfc',
            'password' => ['required', Password::min(8)->mixedCase()->numbers()->symbols()]
        ];
    }

    public function messages(): array
    {
        return [
            'email' => 'Email should have format as example@email.com',
            'password.min' => 'Password should have minimum 8 symbols.',
            'password.mixedCase' => 'Password should have at least one
             uppercase and one lowercase letter',
            'password.numbers' => 'Password should Password should have minimum  1 numbers.',
            'password.symbols' => 'Password should Password should have minimum  1 letter.'
        ];
    }
}
