<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 *
 * @property string $id
 * @property string $address
 * @property float $longitude
 * @property float $latitude
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin Builder
 */
class UsersAddress extends Model
{
    use  HasFactory;

    protected $fillable = [
        'id',
        'user_id',
        'longitude',
        'latitude',
        'address',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
