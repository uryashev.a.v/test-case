<?php

namespace App\Services;

use App\Models\User;
use App\Models\UsersAddress;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response as HTTP_RESPONSE;
use Throwable;
use function Ramsey\Uuid\v4;

class UserService
{
    public function create(User $user): User
    {
        return User::create([
            'id' => v4(),
            'email' => $user->email,
            'password' => $this->getHashPassword($user->password),
            'remember_token' => Str::random(10),
        ]);
    }

    /**
     * @throws Throwable
     */
    public function getTokens($email, $password)
    {
        $response = Http::asForm()->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ])
            ->post('http://127.0.0.1:8001/oauth/token', [
                    'grant_type' => 'password',
                    'client_id' => env('CLIENT_ID'),
                    'client_secret' => env('CLIENT_SECRET'),
                    'username' => $email,
                    'password' => $password,
                    'scope' => ''
            ]);

        return $response->json();
    }
    /**
     * @throws Throwable
     */
    public function refreshTokens($refreshToken)
    {
        $response = Http::asForm()->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ])
            ->post('http://127.0.0.1:8003/oauth/token', [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken,
                    'client_id' => '96db3c1c-0644-474b-8baf-b6eb4b357fb1',
                    'client_secret' => 'wQvARHIudRt8XGz5HyjTXOjxm3gHLrAoLgJBC6qb',
                    'scope' => ''
            ]);

        return $response->json();
    }

    public function getHashPassword($password): string
    {
        return Hash::make($password);
    }

    public function getUserByEmail(string $email)
    {
        return User::where(['email' => $email])->first();
    }

    public function getById(string $id): ?User
    {
        return User::where(['id' => $id])->first();
    }

    public function authorizedUserByPassword($email, $password)
    {
        $user = $this->getUserByEmail($email);
        if (!Hash::check($password, $user->password)) {
            return response()->json([
                'type' => 'USER_IS_NOT_AUTHORIZED',
                'code' => HTTP_RESPONSE::HTTP_NOT_FOUND,
                'message' => 'Пользователь с таким email не найден'
            ], HTTP_RESPONSE::HTTP_NOT_FOUND);
        }
        return $user;
    }

    public function authUser($user)
    {
        auth()->login($user);
        return auth();
    }

    public function getAllAddressesOfUser($userId): LengthAwarePaginator
    {
        return UsersAddress::select('address', 'created_at')
            ->where('user_id', $userId)
            ->paginate(2);

    }

}
