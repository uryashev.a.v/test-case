<?php

namespace App\Jobs;

use App\Http\Controllers\UserController;
use App\Http\Requests\GeoRequest;
use App\Models\UsersAddress;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Yandex\Geocode\Facades\YandexGeocodeFacade;

class GeolocationJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var GeoRequest
     */
    private $longitude;
    private $latitude;
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($longitude, $latitude, $id)
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {

        $geo = YandexGeocodeFacade::setPoint( $this->longitude, $this->latitude)->load();
        // $geo = YandexGeocodeFacade::setPoint( 61.370275, 61.370275)->load();

        UsersAddress::where('id', $this->id)->update([
            'address' => current($geo->getResponse()->getData()),
        ]);
       // var_dump(current($geo->getResponse()->getData()));
        return $geo->getResponse()->getData();
    }

}
